package com.example.android.mockito_demo;

/**
 * Created by parth on 4/4/18.
 */

public interface Warehouse {
    boolean hasInventory(String product, int quantity);
    void remove(String product, int quantity);
}
