package com.example.android.mockito_demo;

import java.util.HashMap;

/**
 * Created by parth on 4/4/18.
 */

public class RealWarehouse implements Warehouse {
    private HashMap products;

    public RealWarehouse() {
        products = new HashMap();
        products.put("Talisker", 5);
        products.put("Lagavulin", 2);
    }

    public boolean hasInventory(String product, int quantity) {
        return inStock(product) >= quantity;
    }

    public void remove(String product, int quantity) {
        products.put(product, inStock(product) - quantity);
    }

    private int inStock(String product) {
        Integer quantity = (Integer) products.get(product);
        return quantity == null ? 0 : quantity;
    }


}
